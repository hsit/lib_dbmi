# Getting started with DBMI Webservice library

With this library you can get intensity quakes, getting data from DBMI infrastructure.

You can search quake by Event ID (int unique event -earthquake- identifier).

The library searchs the corrisponding DBMI quake and try to get informations about:
- Contributor
- Contributors' Analysis
- Localities intensity

## Install and usage

See wiki about Install, Autoload and Usage here: https://gitlab.rm.ingv.it/dbmi/webservice-library/-/wikis/Home

## Documentation
You can find documentation, automatically created by PHPDocumentor here: http://dbmi.gitpages.rm.ingv.it/webservices-library/

## Test and Deploy
Tests are realized using PHPUnit.

## Contributing
If you want to contribute, please use pull requests.
To get a best integration, please code using Behavior Driven Development (BDD), Test Driven Development (TDD) and Calisthenic Programming.

## Authors and acknowledgment
Diego Sorrentino, Istituto Nazionale di Geofisica e Vulcanologia, https://www.ingv.it/organizzazione/chi-siamo/personale/#922

## License
GPL v3

## Project status
Development in progress
