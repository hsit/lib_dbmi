<?php
namespace Dbmi\Webservice;

/**
 * Contributors
 * 
 * @param string 	$contributor
 * @param int|null	$mdpSetID
 */
class Contributor{
	private string $contributor;
	private ?int    $mdpSetID = null;

	public function __construct(
			string $contributor, 
			?int $mdpSetID = null){
		$this->contributor = $contributor;
		$this->contributorMdpSetID = $mdpSetID;	
	}

	/**
	 * Get contributor name
	 * 
	 * @return string Contributor name
	 */
	public function getName():string { return $this->contributor; }

	/**
	 * Get Mdp Set ID
	 * 
	 * @return string|null mdp set id
	 */
	public function getMdpSetID():?string { return $this->mdpSetID; }

}
?>
