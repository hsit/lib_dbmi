<?php
namespace Dbmi\Webservice;

/**
 * Contributor Analysis
 * 
 * @param string	$typeAnalysis
 * @param array|null	$analysis;
 */
class ContributorAnalysis{
	private string $typeAnalysis;
	private ?array	$analysis = null;

	public function __construct(string $typeAnalysis){
		if(empty($typeAnalysis))
			throw new \InvalidArgumentException("ContributorAnalysis is empty");
	
		$this->typeAnalysis = $typeAnalysis;
	}

	/**
	 * Get Analysys type
	 * 
	 * @return string|null analysis type
	 */
	public function getTypeAnalysis():string { return $this->typeAnalysis; }

	/**
	 * Add Analysys type, can be called multiple times
	 * 
	 * @param string type of analysis to search
	 */
	public function addAnalysis(string $analysisString):void { $this->analysis[] = $analysisString; }

	/**
	 * Get Analysys by index
	 * 
	 * @param int index value of the analysis
	 * @return string|null analysis type
	 */
	public function getAnalysisByIndex(int $index):?string { return isset($this->analysis[$index]) ? $this->analysis[$index] : null; }

	/**
	 * Get Analysys type in CSV format
	 * 
	 * @return string|null analysis type in CSV format
	 */
	public function getAnalysisCSV():string {return implode(',', $this->analysis);}

	/**
	 * Get number of Analysys found
	 * 
	 * @return int analysis found
	 */
	public function countAnalysis():int { return is_array($this->analysis) ? count($this->analysis) : 0; }

}
?>
