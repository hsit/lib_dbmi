<?php
namespace Dbmi\Webservice;

use Fdsn\DataStructure\Quake as DS_Quake;
use Fdsn\DataStructure\LatLon as DS_LatLon;

/**
 * DBMI lib to connect to emidius.mi.ingv.it and request all data available for a quake
 * 
 * @param Quake $eventID (nullable) QuakeID in format DBMI
 */
class DBMI {
	private DS_Quake $FdsnQuake;

	private Quake $eventID;
	private Contributor $preferredContributor;

	private bool $dataExists = false;
	private array  $localityDataArray;
	private array  $contributorsAnalysis;  //store ContributorAnalysis objects

	private string $downloadedcontributorData;

	private array $EmidiusURLTemplates = array(
		'conversion'	=> 'https://emidius.mi.ingv.it/services/rosetta/?type=event&input=hsit&output=cpti&format=json&parameters=yes&id=%s',
		'event'		=> 'https://emidius.mi.ingv.it/services/macroseismic/query?eventid=%s&format=text',
		'contributor'	=> 'https://emidius.mi.ingv.it/services/macroseismic/query?eventid=%s&includeallmdpsets=true&format=textmacro',
		'data'		=> 'https://emidius.mi.ingv.it/services/macroseismic/query?format=textmacro&includemdps=true&eventid=%s&contributor=%s',
                'data_by_mdpid' => 'https://emidius.mi.ingv.it/services/macroseismic/query?format=textmacro&includemdps=true&eventid=%s&contributor=%s&mdpsetid=%d',
	);

	private array $EmidiusCompiledURLs = array(
		'conversion'	=> '',
		'event	'	=> '',
		'contributor'	=> '',
		'data'		=> '',
	);


	public function __construct(Quake $eventID = null){
		if ( ! is_null($eventID) )
			$this->eventID = $eventID;
	
		$this->contributorsAnalysis = array(
			'MCS' => new ContributorAnalysis('MCS'),
			'EMS' => new ContributorAnalysis('EMS'),
		);
	}

	/** 
	 * set FDSN quake structure
	 * 
 	 * @param Fdsn\DataStructure\Quake $FdsnQuake 	Quake data structure 
	 */
	public function setFdsnQuake(DS_Quake $FdsnQuake){ $this->FdsnQuake = $FdsnQuake;}
	
	
	/** 
	 * string $contributor set already choose contributor
	 * int $mdpSetID|null set already choose mdp set id
	 * 
	 * @return void
	 */
	public function setPreferredContributor(Contributor $contributor): void{
		$this->preferredContributor = $contributor;
	}

	/**
	 * get preferred contributor
	 * 
	 * @return string the preferred contributor
	 */
	public function getPreferredContributor(){
		return $this->preferredContributor->getName();
	}

	/**
	 * check if data exists
	 * 
	 * @return bool true if data exists, false otherwise
	 */
	public function dataExists(): bool{
		return count($this->localityDataArray) > 0;
	}


	/**
	 * Try to find the seismic event in DBMI database and its internal ID
	 *
	 * @return Quake|null eventID (and property eventID is set)
	 */
	public function findEventID(): Quake{
		$this->EmidiusCompiledURLs['conversion'] = sprintf($this->EmidiusURLTemplates['conversion'], $this->FdsnQuake->eventID());

		//error_log("Emidius id converter url: {$this->EmidiusCompiledURLs['conversion']}");

		$curlSession = curl_init();
		curl_setopt_array($curlSession, array(
			CURLOPT_URL 		=> $this->EmidiusCompiledURLs['conversion'],
			CURLOPT_HEADER 		=> false,
			CURLOPT_CUSTOMREQUEST	=> 'GET',
			CURLOPT_RETURNTRANSFER	=> true,
			CURLOPT_FAILONERROR     => true
			)
		);
		$downloadedData = curl_exec($curlSession);
		
		$errorCode = curl_errno($curlSession);
		$errorInfo = curl_error($curlSession);
		curl_close($curlSession);
		
		if(CURLE_OK != $errorCode){
			error_log("[$errorCode] $errorInfo ====");
			return null;
		}

		$response = json_decode($downloadedData, true);
		
		if(	! is_array($response) 
			|| ! array_key_exists('output', $response)
			|| ! array_key_exists('id', $response['output'])
			|| empty($response['output']['id'])){
//				error_log(sprintf("ONT id: %d => DBMI id: %s",
//					$this->FdsnQuake->eventID(),
//					$response['output']['id']));
				return null;
		}

		$this->eventID = new Quake($response['output']['id']);

		return $this->eventID;
	}

	/**
	 * Get event epicentral details
	 *
	 * @return Quake|null eventID (and property eventID is set)
	 */
	public function findEventDetails():array{
		if( ! isset($this->eventID) )
			throw new \RuntimeException("Quake not set");

		$this->EmidiusCompiledURLs['event'] = sprintf($this->EmidiusURLTemplates['event'], $this->eventID->getID());

		//error_log("Emidius id converter url: {$this->EmidiusCompiledURLs['conversion']}");

		$curlSession = curl_init();
		curl_setopt_array($curlSession, array(
			CURLOPT_URL 		=> $this->EmidiusCompiledURLs['event'],
			CURLOPT_HEADER 		=> false,
			CURLOPT_CUSTOMREQUEST	=> 'GET',
			CURLOPT_RETURNTRANSFER	=> true,
			CURLOPT_FAILONERROR     => true
			)
		);
		$downloadedData = curl_exec($curlSession);
		
		$errorCode = curl_errno($curlSession);
		$errorInfo = curl_error($curlSession);
		curl_close($curlSession);
		
		if(CURLE_OK != $errorCode){
			error_log("[$errorCode] $errorInfo ====");
			return null;
		}

		$rows = preg_split("/\n/", trim($downloadedData));

		if( 2 != count($rows) ) //header+data
			throw new \RuntimeException('Invalid epicenter response');
	
		$details = preg_split("/\|/", $rows[1]);	

		return $details;
	}

	/**
	 * find studies based on eventID
	 *
	 * @return array|null all contributors, divided in array by MCS|EMS
	 */
	public function findContributors(): array{
		//ticket #543, added HSIT preferred data contributor
		if(is_null($this->eventID))
			return null;

		$this->EmidiusCompiledURLs['contributor'] = sprintf($this->EmidiusURLTemplates['contributor'], $this->eventID->getID());	

		//error_log("EmidiusContributorURL: {$this->EmidiusCompiledURLs['contributor']}");

		$curlSession = curl_init();
		curl_setopt_array($curlSession, array(
					CURLOPT_URL 		=> $this->EmidiusCompiledURLs['contributor'],
					CURLOPT_HEADER 		=> false,
					CURLOPT_CUSTOMREQUEST	=> 'GET',
					CURLOPT_RETURNTRANSFER	=> true,
					CURLOPT_FAILONERROR     => true
					)
				);
		$this->downloadedcontributorData = curl_exec($curlSession);

		$errorCode = curl_errno($curlSession);
		$errorInfo = curl_errno($curlSession);
		curl_close($curlSession);

		if(CURLE_OK != $errorCode){
			error_log("[$errorCode] $errorInfo ====");
			return null;
		}

		return $this->splitContributorsByIntensityTypeAnalisys();
	}

	/**
	 * select best contributor
	 */
	public function chooseContributor(){
		$totMCSContributors = $this->contributorsAnalysis['MCS']->countAnalysis();
		$totEMSContributors = $this->contributorsAnalysis['EMS']->countAnalysis();
		switch($totMCSContributors){
			case 0: //ONLY EMS
				$this->preferredContributor  = new Contributor($this->contributorsAnalysis['EMS']->getAnalysisByIndex(0));
				break;
			case 1: //EXISTS one MCS study
				$this->preferredContributor  = new Contributor($this->contributorsAnalysis['MCS']->getAnalysisByIndex(0));
				break;
			default: 
				$message = sprintf("Per l'evento %d ci sono diversi contributor:\n%s\nPuoi ricercarli su Emidius con il seguente procedimento:\n%s \n", 
					$this->FdsnQuake->eventID(), 
					$this->contributorsAnalysis['MCS']->getAnalysisCSV(), 
					implode(PHP_EOL, $this->EmidiusCompiledURLs));
				error_log($message);
				break;
			
		}
	}

	/**
	 * download data for a eventID -> preferred contributor
	 */
	public function downloadData(): array{
		//ticket #543, added HSIT preferred data contributor
                if(empty($this->preferredContributor->getMdpSetID()))
                        $this->EmidiusCompiledURLs['data'] = sprintf($this->EmidiusURLTemplates['data'], $this->eventID->getID(), $this->preferredContributor->getName());
                else
                        $this->EmidiusCompiledURLs['data'] = sprintf($this->EmidiusURLTemplates['data_by_mdpid'], $this->eventID->getID(), $this->preferredContributor->getName(), $this->preferredContributor->getMdpSetID());


		//error_log("EmidiusDataURL: {$this->EmidiusCompiledURLs['data']}");

		$curlSession = curl_init();
		curl_setopt_array($curlSession, array(
					CURLOPT_URL 		=> $this->EmidiusCompiledURLs['data'],
					CURLOPT_HEADER 		=> false,
					CURLOPT_CUSTOMREQUEST	=> 'GET',
					CURLOPT_RETURNTRANSFER	=> true,
					CURLOPT_FAILONERROR     => true
					)
				);
		$downloadedData = curl_exec($curlSession);

		$errorCode = curl_errno($curlSession);
		$errorInfo = curl_errno($curlSession);
		curl_close($curlSession);

		if(CURLE_OK != $errorCode){
			error_log("[$errorCode] $errorInfo ====");
			return null;
		}

		$stringsData = preg_split("/\n/", $downloadedData);
		$stringsArray = array_filter($stringsData, array($this, 'filterEmptyDataLine'));


		if(0 == count($stringsArray)){
			$this->dataExists = false;
			return null;
		}
		
		$this->dataExists = true;
		foreach($stringsArray as $data){	
			$dataArray = str_getcsv($data, '|');
			
			$this->localityDataArray[] = new Locality(
				$dataArray[8],
				$dataArray[9],
				new DS_LatLon($dataArray[10], $dataArray[11]),
				new Intensity($dataArray[12])
			);
		}

		return $this->localityDataArray;
	}

	/**
	 * filter empty lines or comment lines (starting with #)
	 */
	private function filterEmptyDataLine($data){
		return !(preg_match('/^$/', $data) || preg_match("/^#/", $data));
	}

	/** 
	 * filter rows with no mdpset	
	 */
	private function filterRowsWithMdpSet($row){
		return preg_match('@.*mdpset/(.*)/\d*@', $row);
	}

	/**
	 * divide found contributors by intensity-type of study
	 */
	private function splitContributorsByIntensityTypeAnalisys(): array{
		$originaldataArray = preg_split("/\n/", $this->downloadedcontributorData);
		$originalRows = count($originaldataArray);

		$dataArray = array_filter($originaldataArray, array($this, 'filterRowsWithMdpSet'));
		if(0 == count($dataArray)){
			error_log("No contributor data (downloaded {$originalRows}) but no one has mdpset data");
			return null; //nothing to do
		}

		foreach($dataArray as $data){
			$IntensitesRow = str_getcsv($data, '|');
			$key = ('MCS' == $IntensitesRow[6]) ? 'MCS' : 'EMS';

			preg_match('@.*mdpset/(.*)/\d*@', $IntensitesRow[1], $matchArray);
			$this->contributorsAnalysis[$key]->addAnalysis($matchArray[1]);
		}

		return $this->contributorsAnalysis;
	}


}

?>
