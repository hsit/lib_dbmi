<?php
namespace Dbmi\Webservice;

/**
 * Intensity
 * 
 * @param string	$value  DBMI intensity non numeric
 */
class Intensity{
	private string $value;
	private float $numericValue;

	public function __construct( string $value){
		if('' == $value)
			throw new \InvalidArgumentException("value is empty");
		
		$this->numericValue = $this->convertToNumeric($value);
	}

	/**
	 * Get intensity value as added  (raw)
	 * 
	 * @return string Intensity value
	 */
	public function getValue():string { return $this->value; }

	/**
	 * Get numeric intensity value 
	 * 
	 * @return float intensity value
	 */
	public function getNumericValue():float { return $this->numericValue; }

	/**
	 * Convert intensity string to a numeric value
	 * 
	 * @param string value to convert
	 * @return float intensity value converted
	 */
	private function convertToNumeric(string $value):float {
		// $value is simply numeric
		if(is_numeric($value))
			return intval($value);

		// $value is a middle grade like x-(x+1)
		if(preg_match('/(\d*)-(\d*)/', $value, $matches))
			return ($matches[1] + $matches[2]) / 2;

		// $value is a special string
		switch(strtoupper($value)){
			case 'RS': 
			case 'NR': 
			case 'W': 
			case 'E':  return  0;   break;
			case 'G':  return  0.2; break;
			case 'NF': return  1;   break;
			case 'NC': return  1.8; break;
			case 'SF': return  2.9; break;
			case 'F':  return  3.9; break;
			case 'HF': return  5.1; break;
			case 'SD': return  5.6; break;
			case 'D':  return  6.4; break;
			case 'HD': return  8.6; break;
			default:   return -1;   break;
		}
	}
	
}
?>
