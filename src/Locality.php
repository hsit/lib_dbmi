<?php
namespace Dbmi\Webservice;

use Fdsn\DataStructure\LatLon as DS_LatLon;

/**
 * Locality 
 * 
 * @param string			$id
 * @param string			$name
 * @param Fdsn\DataStructure\LatLon 	$point
 * @param Intensity			$intensity
 */
class Locality{
	private string $id;
	private string $name;
	private DS_LatLon $point;
	private Intensity $intensity;

	public function __construct(
			string $id, 
			string $name,
			DS_LatLon $point,
			Intensity $intensity){
		if(empty($id))
			throw new \InvalidArgumentException("ID is empty");

		if(empty($name))
			throw new \InvalidArgumentException("Name is empty");

		$this->id = $id;
		$this->name = $name;
		$this->point = $point;
		$this->intensity = $intensity;
	}

	public function id():string { return $this->id; } 
	public function name():string { return $this->name; } 
	public function point():DS_LatLon{ return $this->point; } 
	public function intensity():Intensity{ return $this->intensity; } 
}
?>
