<?php
namespace Dbmi\Webservice;

/**
 * Contributors
 * 
 * @param string 	$id
 */
class Quake{
	private string $id;

	public function __construct( string $id ){
		if( ! $this->isValid($id) )
			throw new \InvalidArgumentException("Quake::id not valid");
		
		$this->id = $id;
	}

	/**
	 * Get checked event id
	 * 
	 * @return string eventi id
	 */
	public function getID():string { return $this->id; }


	/**
	 * Check if event id is valid
	 * 
	 * @param string event id
	 * @return bool TRUE if event id is valid, FALSE otherwise
	 */
	private function isValid(string $id): bool {
		return preg_match('/\d{6}_\d{4}_\d{3}/', $id);
	}

}
?>
