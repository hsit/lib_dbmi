<?php 
declare(strict_types=1);
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

use Dbmi\Webservice\DBMI;
use Dbmi\Webservice\Quake as DBMI_Quake;

use Fdsn\DataStructure\Author;
use Fdsn\DataStructure\Depth;
use Fdsn\DataStructure\Epicenter;
use Fdsn\DataStructure\LatLon;
use Fdsn\DataStructure\Location;
use Fdsn\DataStructure\Magnitude;
use Fdsn\DataStructure\Province;
use Fdsn\DataStructure\Quake as DS_Quake;

class DBMIFromFDSNQuakeTest extends TestCase{
	private $obj; 

	protected function setUp():void {
		$this->obj = new DBMI();
		$this->obj->setFdsnQuake(new DS_Quake(8863681, 
					new \DateTime('2004-02-12T15:19:21+00:00'), 
					new Location('Da qualche parte'),
					new Magnitude('mw', 5), 
					new Epicenter(new LatLon(10,10), new Depth(5)),
					new Author('test')
				));
	}

	public function testFindContributor(): void{
		$this->obj->findEventID();
		$res = $this->obj->findContributors();
		$this->assertSame($res['MCS']->getAnalysisByIndex(0), 'GALAL017');
		$this->assertSame($res['EMS']->getAnalysisByIndex(0), 'ROSAL019');
		$this->assertSame($res['EMS']->getAnalysisByIndex(1), 'TERAL016a');
	}

	public function testBestContributorFound(): void{
		$this->obj->findEventID();
		$this->obj->findContributors();
		$this->obj->chooseContributor();
		$this->assertSame('GALAL017', $this->obj->getPreferredContributor());
	}

	public function testGetData(): void{
		$this->obj->findEventID();
		$this->obj->findContributors();
		$this->obj->chooseContributor();
		$res = $this->obj->downloadData();
		$this->assertCount(464, $res);
	}

}

