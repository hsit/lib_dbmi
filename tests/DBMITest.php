<?php 
declare(strict_types=1);
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

use Dbmi\Webservice\DBMI;
use Dbmi\Webservice\Quake as DBMI_Quake;

use HSIT\DataStructure\Author;
use HSIT\DataStructure\Depth;
use HSIT\DataStructure\Epicenter;
use HSIT\DataStructure\Istat;
use HSIT\DataStructure\LatLon;
use HSIT\DataStructure\Location;
use HSIT\DataStructure\Magnitude;
use HSIT\DataStructure\Province;
use HSIT\DataStructure\Quake as DS_Quake;

class DBMITest extends TestCase{
	private $obj; 

	protected function setUp():void {
		$this->obj = new DBMI(
			new DS_Quake(8863681, 
					new \DateTime('2004-02-12T15:19:21+00:00'), 
					new Location('Da qualche parte', new Istat(12345), new LatLon(10,10), new Province('RM', 'Roma')), 
					new Magnitude('mw', 5), 
					new Epicenter(new LatLon(10,10), new Depth(5)),
					new Author('test')
				) 
		);
	}

	public function testEventId(): void {
		$dbmiQuake = new DBMI_Quake('20161030_0640_000');
		$this->assertEquals($dbmiQuake->getID(), $this->obj->findEventID()->getID());
	}

	public function testFindContributor(): void{
		$this->obj->findEventID();
		$res = $this->obj->findContributors();
		$this->assertSame($res['MCS']->getAnalysisByIndex(0), 'GALAL017');
		$this->assertSame($res['EMS']->getAnalysisByIndex(0), 'ROSAL019');
		$this->assertSame($res['EMS']->getAnalysisByIndex(1), 'TERAL016a');
	}

	public function testBestContributorFound(): void{
		$this->obj->findEventID();
		$this->obj->findContributors();
		$this->obj->chooseContributor();
		$this->assertSame('GALAL017', $this->obj->getPreferredContributor());
	}

	public function testGetData(): void{
		$this->obj->findEventID();
		$this->obj->findContributors();
		$this->obj->chooseContributor();
		$res = $this->obj->downloadData();
		$this->assertCount(464, $res);
	}

}

