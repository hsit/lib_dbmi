<?php
declare(strict_types=1);
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

use Dbmi\Webservice\Intensity;

class IntensityTest extends TestCase{

	public static function dataProvider(){
		return [
			['0', 		0],
			['1', 		1],
			['2', 		2],
			['3', 		3],
			['4', 		4],
			['5', 		5],
			['6', 		6],
			['7', 		7],
			['8', 		8],
			['9', 		9],
			['10',		10],
			['11', 		11],
			['12', 		12],
			['0-1', 	0.5],
			['1-2', 	1.5],
			['2-3', 	2.5],
			['3-4', 	3.5],
			['4-5', 	4.5],
			['5-6', 	5.5],
			['6-7', 	6.5],
			['7-8', 	7.5],
			['8-9', 	8.5],
			['9-10', 	9.5],
			['10-11', 	10.5],
			['11-12', 	11.5],
			['RS', 		0],
			['NR', 		0],
			['W', 		0],
			['E',  		0],
			['G',  		0.2],
			['NF', 		1],
			['NC', 		1.8],
			['SF', 		2.9],
			['F',  		3.9],
			['HF', 		5.1],
			['SD', 		5.6],
			['D',  		6.4],
			['HD', 		8.6],
		];
	}

	/**
         * @dataProvider dataProvider
         */
        public function testIntensity(string $value, float $expected): void{
                $obj = new Intensity($value);
                $this->assertSame($obj->getNumericValue(), $expected);
        }



}

?>
